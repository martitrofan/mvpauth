package ru.bars_open.mpv_example.utils;

public interface ConstantManager {

    String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";

    String PRODUCT_KEY = "PRODUCT_KEY";
}
