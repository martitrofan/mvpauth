package ru.bars_open.mpv_example.ui.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import ru.bars_open.mpv_example.MvpApplication;
import ru.bars_open.mpv_example.R;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.components.DaggerPicassoComponent;
import ru.bars_open.mpv_example.di.components.PicassoComponent;
import ru.bars_open.mpv_example.di.modules.PicassoCacheModule;
import ru.bars_open.mpv_example.di.scopes.RootScope;
import ru.bars_open.mpv_example.mvp.presenters.RootPresenter;
import ru.bars_open.mpv_example.mvp.views.IRootView;
import ru.bars_open.mpv_example.ui.fragments.AccountFragment;
import ru.bars_open.mpv_example.ui.fragments.CatalogFragment;
import ru.bars_open.mpv_example.BuildConfig;
import ru.bars_open.mpv_example.ui.custom_views.CircleImageView;



public class RootActivity extends BaseActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    Picasso mPicasso;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;
    @BindView(R.id.basket)
    ImageButton mBasketBtn;
    @BindView(R.id.basket_indicator)
    ImageView mBasketIndicator;

    FragmentManager mFragmentManager;

    //region ======================== Activity Lifecycle ========================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        setupToolbar();
        setupDrawer();
        mRootPresenter.takeView(this);
        mRootPresenter.initView();
        // TODO: 10.11.2016 init view

        mBasketBtn.setOnClickListener(this);

        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }

    //endregion

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void setupDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
        setupCircleAvatar();
    }

    private void setupCircleAvatar() {
        View headerLayout = mNavigationView.getHeaderView(0);
        final CircleImageView avatarImg = (CircleImageView) headerLayout.findViewById(R.id.circle_avatar);
        mPicasso.load(R.drawable.user_avatar)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.user_avatar)
                .into(avatarImg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        mPicasso.load(R.drawable.user_avatar)
                                .fit()
                                .centerCrop()
                                .placeholder(R.drawable.user_avatar)
                                .into(avatarImg);
                    }
                });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        int checkedItem = 0;
        switch (item.getItemId()) {
            case R.id.nav_account:
                fragment = new AccountFragment();
                checkedItem = R.id.nav_account;
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                checkedItem = R.id.nav_catalog;
                break;
            case R.id.nav_favourite:
                checkedItem = R.id.nav_favourite;
                break;
            case R.id.nav_orders:
                checkedItem = R.id.nav_orders;
                break;
            case R.id.nav_notifications:
                checkedItem = R.id.nav_notifications;
                break;
        }
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        if (checkedItem != 0) {
            mNavigationView.setCheckedItem(checkedItem);
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (mFragmentManager.getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.alert_dialog_exit_title)
                    .setPositiveButton(R.string.alert_dialog_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.alert_dialog_no, null)
                    .create();
            alertDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.basket) {
            // TODO: 31.10.2016 show cart fragment
        }
    }

    //region ======================== IRootView ========================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.sorry_something_wrong));
            // TODO: 23.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    //endregion

    public void showBasketIndicator() {
        mBasketIndicator.setVisibility(View.VISIBLE);
    }

    public void showAuthFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, new Fragment())
                .addToBackStack(null)
                .commit();
    }

    //region ======================== DI ========================

    private Component createDaggerComponent() {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerPicassoComponent.builder()
                    .appComponent(MvpApplication.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registerComponent(PicassoComponent.class, picassoComponent);
        }
        return DaggerRootActivity_Component.builder()
                .picassoComponent(picassoComponent)
                .module(new Module())
                .build();
    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = Module.class)
    @RootScope
    public interface Component {
        void inject(RootActivity activity);

        RootPresenter getRootPresenter();
    }

    @dagger.Module
    public class Module {
        @Provides
        @RootScope
        RootPresenter provideRootPresenter() {
            return new RootPresenter();
        }
    }

    //endregion
}
