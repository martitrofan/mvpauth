package ru.bars_open.mpv_example.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import me.relex.circleindicator.CircleIndicator;
import ru.bars_open.mpv_example.R;
import ru.bars_open.mpv_example.data.storage.dto.ProductDto;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.scopes.CatalogScope;
import ru.bars_open.mpv_example.mvp.presenters.CatalogPresenter;
import ru.bars_open.mpv_example.mvp.views.ICatalogView;
import ru.bars_open.mpv_example.ui.activities.RootActivity;
import ru.bars_open.mpv_example.ui.adapters.CatalogAdapter;

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {

    @Inject
    CatalogPresenter mPresenter;

    @BindView(R.id.add_to_cart_btn)
    Button mAddToCartBtn;
    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;

    public CatalogFragment() {

    }

    //region ======================== Fragment Lifecycle ========================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);

        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        mPresenter.takeView(this);
        mPresenter.initView();
        mAddToCartBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //endregion

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_to_cart_btn) {
            mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
        }
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    //region ======================== ICatalogView ========================

    @Override
    public void showCatalogView(List<ProductDto> productsList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product : productsList) {
            adapter.addItem(product);
        }
        mProductPager.setAdapter(adapter);
        mIndicator.setViewPager(mProductPager);
    }

    @Override
    public void showAuthScreen() {
        getRootActivity().showAuthFragment();
    }

    @Override
    public void showCartIndicator() {
        getRootActivity().showBasketIndicator();
    }

    @Override
    public void updateProductCounter() {
        // TODO: 31.10.2016 update count product on cart icon
    }

    //endregion

    //region ======================== DI ========================

    private Component createDaggerComponent() {
        return DaggerCatalogFragment_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogFragment fragment);
    }

    //endregion
}
