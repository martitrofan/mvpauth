package ru.bars_open.mpv_example.ui.activities;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import ru.bars_open.mpv_example.BuildConfig;
import ru.bars_open.mpv_example.R;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.scopes.AuthScope;
import ru.bars_open.mpv_example.mvp.presenters.AuthPresenter;
import ru.bars_open.mpv_example.mvp.views.IAuthView;
import ru.bars_open.mpv_example.ui.custom_views.AuthPanel;

public class SplashActivity extends BaseActivity implements IAuthView, View.OnClickListener {

    @Inject
    AuthPresenter mPresenter;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.app_name_txt)
    TextView mNameTv;
    @BindView(R.id.auth_panel)
    AuthPanel mAuthPanel;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.vk_btn)
    ImageButton mVkBtn;
    @BindView(R.id.facebook_btn)
    ImageButton mFacebookBtn;
    @BindView(R.id.twitter_btn)
    ImageButton mTwitterBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;

    //region ======================== Activity Lifecycle ========================

    public static Component createDaggerComponent() {
        return DaggerSplashActivity_Component.builder()
                .module(new Module())
                .build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        mPresenter.takeView(this);
        mPresenter.initView();

        mLoginBtn.setOnClickListener(this);
        mVkBtn.setOnClickListener(this);
        mFacebookBtn.setOnClickListener(this);
        mTwitterBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "PTBebasNeueBook.ttf");
        ((TextView) findViewById(R.id.app_name_txt)).setTypeface(typeface);
    }

    //endregion

    //region ======================== IRootView ========================

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        if (isFinishing()) {
            DaggerService.unregisterScope(AuthScope.class);
        }
        super.onDestroy();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.sorry_something_wrong));
            // TODO: 23.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    //endregion

    //region ======================== IAuthView ========================

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public void animateVkBtn() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_anim);
        mVkBtn.startAnimation(animation);
    }

    @Override
    public void animateFacebookBtn() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_anim);
        mFacebookBtn.startAnimation(animation);
    }

    @Override
    public void animateTwitterBtn() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_anim);
        mTwitterBtn.startAnimation(animation);
    }

    //endregion

    @Override
    public void showCatalogScreen() {
        Intent intent = new Intent(this, RootActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.vk_btn:
                mPresenter.clickOnVk();
                break;
            case R.id.facebook_btn:
                mPresenter.clickOnFacebook();
                break;
            case R.id.twitter_btn:
                mPresenter.clickOnTwitter();
                break;
        }
    }

    //region ======================== DI ========================

    @Override
    public void onBackPressed() {
        if (!mAuthPanel.isIdle()) {
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(SplashActivity activity);

        void inject(AuthPanel authPanel);
    }

    @dagger.Module
    public static class Module {

        @Provides
        @AuthScope
        AuthPresenter provideAuthPresenter() {
            return new AuthPresenter();
        }
    }

    //endregion
}
