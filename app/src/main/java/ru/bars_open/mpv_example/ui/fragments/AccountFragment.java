package ru.bars_open.mpv_example.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import ru.bars_open.mpv_example.R;

public class AccountFragment extends Fragment {

    public AccountFragment() {

    }

    //region ======================== Fragment Lifecycle ========================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    //endregion

}
