package ru.bars_open.mpv_example.ui.custom_views;


import android.animation.LayoutTransition;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bars_open.mpv_example.R;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.mvp.presenters.AuthPresenter;
import ru.bars_open.mpv_example.ui.activities.SplashActivity;

public class AuthPanel extends LinearLayout {
    public static final int IDLE_STATE = 0;
    public static final int LOGIN_STATE = 1;

    private static final String TAG = "AuthPanel";

    @Inject
    AuthPresenter mAuthPresenter;

    private int mCustomState = IDLE_STATE;
    private boolean isEmailValid;
    private boolean isPasswordValid;

    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailLayout;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordLayout;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;

    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);

        SplashActivity.Component component = DaggerService.getComponent(SplashActivity.Component.class);
        if (component == null) {
            component = SplashActivity.createDaggerComponent();
            DaggerService.registerComponent(SplashActivity.Component.class, component);
        }
        component.inject(this);

        setTextWatchers();
        showViewFromState();
        LayoutTransition layoutTransition = new LayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        this.setLayoutTransition(layoutTransition);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCustomState(savedState.state);
    }

    public void setCustomState(int state) {
        if (state == IDLE_STATE) {
            mEmailEt.setText("");
            mPasswordEt.setText("");
        }
        mCustomState = state;
        showViewFromState();
    }

    private void showLoginState() {
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(GONE);
        mLoginBtn.setEnabled(false);
    }

    private void showIdleState() {
        mAuthCard.setVisibility(GONE);
        mShowCatalogBtn.setVisibility(VISIBLE);
        mLoginBtn.setEnabled(true);
    }

    private void showViewFromState() {
        if (mCustomState == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void setTextWatchers() {
        mEmailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mAuthPresenter.checkEmailValid(s.toString());
            }
        });
        mPasswordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mAuthPresenter.checkPasswordValid(s.toString());
            }
        });
    }

    private void checkLogin() {
        if (!isIdle()) {
            mLoginBtn.setEnabled(isEmailValid && isPasswordValid
                    && mEmailEt.getText().length() != 0
                    && mPasswordEt.getText().length() != 0
            );
        }
    }

    public String getUserEmail() {
        return mEmailEt.getText().toString();
    }

    public String getUserPassword() {
        return mPasswordEt.getText().toString();
    }

    public void setEmailError(String error) {
        if (error.isEmpty()) {
            mEmailLayout.setError("");
            mEmailLayout.setErrorEnabled(false);
            isEmailValid = true;
        } else {
            mEmailLayout.setError(error);
            isEmailValid = false;
        }
        checkLogin();
    }

    public void setPasswordError(String error) {
        if (error.isEmpty()) {
            mPasswordLayout.setError("");
            mPasswordLayout.setErrorEnabled(false);
            isPasswordValid = true;
        } else {
            mPasswordLayout.setError(error);
            isPasswordValid = false;
        }
        checkLogin();
    }

    public boolean isIdle() {
        return mCustomState == IDLE_STATE;
    }

    static class SavedState extends BaseSavedState {
        private int state;

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel source) {
                return new SavedState(source);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            state = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }
    }
}
