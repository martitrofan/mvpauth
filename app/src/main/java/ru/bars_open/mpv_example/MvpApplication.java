package ru.bars_open.mpv_example;


import android.app.Application;
import android.content.Context;

import ru.bars_open.mpv_example.di.components.AppComponent;
import ru.bars_open.mpv_example.di.components.DaggerAppComponent;
import ru.bars_open.mpv_example.di.modules.AppModule;

public class MvpApplication extends Application {

    private static Context sContext;
    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        createComponent();
        sContext = this;
    }

    private void createComponent() {
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    public static Context getContext() {
        return sContext;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }
}
