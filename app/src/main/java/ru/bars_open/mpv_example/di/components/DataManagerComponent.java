package ru.bars_open.mpv_example.di.components;


import javax.inject.Singleton;

import dagger.Component;
import ru.bars_open.mpv_example.data.managers.DataManager;
import ru.bars_open.mpv_example.di.modules.LocalModule;
import ru.bars_open.mpv_example.di.modules.NetworkModule;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
