package ru.bars_open.mpv_example.di.components;


import javax.inject.Singleton;

import dagger.Component;
import ru.bars_open.mpv_example.di.modules.ModelModule;
import ru.bars_open.mpv_example.mvp.models.AbstractModel;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
