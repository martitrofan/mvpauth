package ru.bars_open.mpv_example.di.components;


import javax.inject.Singleton;

import dagger.Component;
import ru.bars_open.mpv_example.di.modules.AuthPresenterModule;
import ru.bars_open.mpv_example.mvp.presenters.AuthPresenter;

@Component(modules = AuthPresenterModule.class)
@Singleton
public interface AuthPresenterComponent {
    void inject(AuthPresenter presenter);
}
