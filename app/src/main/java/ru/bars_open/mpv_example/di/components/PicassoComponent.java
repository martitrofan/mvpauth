package ru.bars_open.mpv_example.di.components;


import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import ru.bars_open.mpv_example.di.modules.PicassoCacheModule;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@Singleton
public interface PicassoComponent {
    Picasso getPicasso();
}
