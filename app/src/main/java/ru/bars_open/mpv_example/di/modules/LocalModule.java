package ru.bars_open.mpv_example.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.bars_open.mpv_example.data.managers.PreferencesManager;

@Module
public class LocalModule {
    
    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }
}
