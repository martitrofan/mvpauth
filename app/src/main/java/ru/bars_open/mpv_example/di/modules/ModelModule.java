package ru.bars_open.mpv_example.di.modules;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.bars_open.mpv_example.data.managers.DataManager;

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }
}
