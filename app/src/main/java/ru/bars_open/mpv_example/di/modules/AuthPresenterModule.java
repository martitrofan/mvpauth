package ru.bars_open.mpv_example.di.modules;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.bars_open.mpv_example.mvp.models.AuthModel;

@Module
public class AuthPresenterModule {
    @Provides
    @Singleton
    AuthModel provideAuthModule() {
        return new AuthModel();
    }
}
