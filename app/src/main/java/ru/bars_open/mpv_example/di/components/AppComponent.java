package ru.bars_open.mpv_example.di.components;


import android.content.Context;

import dagger.Component;
import ru.bars_open.mpv_example.di.modules.AppModule;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
