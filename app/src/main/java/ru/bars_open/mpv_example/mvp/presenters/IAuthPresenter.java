package ru.bars_open.mpv_example.mvp.presenters;

public interface IAuthPresenter {

    void clickOnLogin();
    void clickOnVk();
    void clickOnFacebook();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();

    void checkEmailValid(String email);
    void checkPasswordValid(String password);
}
