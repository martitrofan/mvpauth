package ru.bars_open.mpv_example.mvp.views;


import java.util.List;

import ru.bars_open.mpv_example.data.storage.dto.ProductDto;

public interface ICatalogView extends IView {

    void showCatalogView(List<ProductDto> productsList);
    void showAuthScreen();
    void showCartIndicator();

    void updateProductCounter();
}
