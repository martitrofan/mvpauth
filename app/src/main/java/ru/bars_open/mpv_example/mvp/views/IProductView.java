package ru.bars_open.mpv_example.mvp.views;

import ru.bars_open.mpv_example.data.storage.dto.ProductDto;

public interface IProductView extends IRootView {

    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
}
