package ru.bars_open.mpv_example.mvp.presenters;

public interface IProductPresenter {

    void clickOnPlus();
    void clickOnMinus();
}
