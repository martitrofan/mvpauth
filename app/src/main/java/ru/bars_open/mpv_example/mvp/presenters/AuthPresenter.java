package ru.bars_open.mpv_example.mvp.presenters;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.Provides;
import ru.bars_open.mpv_example.R;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.scopes.AuthScope;
import ru.bars_open.mpv_example.mvp.models.AuthModel;
import ru.bars_open.mpv_example.mvp.views.IAuthView;
import ru.bars_open.mpv_example.ui.custom_views.AuthPanel;

public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter {

    @Inject
    AuthModel mModel;

    //region ======================== Email and password patterns ========================

    private static final String emailPattern = "^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%" +
            "&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7" +
            "f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
            "\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" +
            "\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x" +
            "0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])$";

    private static final String passwordPattern = "^\\S{8,}$";

    //endregion

    public AuthPresenter() {
        mModel = new AuthModel();
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if (getView() == null) return;
        if (checkUserAuth()) {
            getView().hideLoginBtn();
        } else {
            getView().showLoginBtn();
        }
    }

    @Override
    public void clickOnLogin() {
        if (getView() == null || getView().getAuthPanel() == null) return;
        AuthPanel authPanel = getView().getAuthPanel();
        if (authPanel.isIdle()) {
            authPanel.setCustomState(AuthPanel.LOGIN_STATE);
        } else {
            // TODO: 23.10.2016 auth user
            mModel.loginUser(authPanel.getUserEmail(), authPanel.getUserPassword());
            getView().showMessage("request for user auth");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView() == null) return;
        getView().animateVkBtn();
        getView().showMessage("clickOnVk");
    }

    @Override
    public void clickOnFacebook() {
        if (getView() == null) return;
        getView().animateFacebookBtn();
        getView().showMessage("clickOnFacebook");
    }

    @Override
    public void clickOnTwitter() {
        if (getView() == null) return;
        getView().animateTwitterBtn();
        getView().showMessage("clickOnTwitter");
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() == null) return;
        // TODO: 30.10.2016 if update data complete start catalog screen
        getView().showCatalogScreen();
    }

    @Override
    public boolean checkUserAuth() {
        return mModel.isAuthUser();
    }

    @Override
    public void checkEmailValid(String email) {
        if (getView() == null || getView().getAuthPanel() == null) return;
        String error = "";
        Matcher matcher = Pattern.compile(emailPattern).matcher(email);
        if (!email.isEmpty() && !matcher.matches()) {
            error = getView().getAuthPanel().getContext().getString(R.string.email_not_valid);
        }
        getView().getAuthPanel().setEmailError(error);
    }

    @Override
    public void checkPasswordValid(String password) {
        if (getView() == null || getView().getAuthPanel() == null) return;
        String error = "";
        Matcher matcher = Pattern.compile(passwordPattern).matcher(password);
        if (!password.isEmpty() && !matcher.matches()) {
            error = getView().getAuthPanel().getContext().getString(R.string.password_not_valid);
        }
        getView().getAuthPanel().setPasswordError(error);
    }

    //region ======================== DI ========================

    @dagger.Module
    public class Module {

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {

        void inject(AuthPresenter presenter);
    }

    private Component createDaggerComponent() {
        return DaggerAuthPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    //endregion
}