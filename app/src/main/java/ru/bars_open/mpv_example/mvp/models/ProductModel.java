package ru.bars_open.mpv_example.mvp.models;


import ru.bars_open.mpv_example.data.storage.dto.ProductDto;

public class ProductModel extends AbstractModel {

    public ProductModel() {

    }

    public ProductDto getProductById(int productId) {
        // TODO: 30.10.2016 get product from datamanager
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }
}
