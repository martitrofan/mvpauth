package ru.bars_open.mpv_example.mvp.presenters;


import javax.inject.Inject;

import dagger.Provides;
import ru.bars_open.mpv_example.data.storage.dto.ProductDto;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.scopes.ProductScope;
import ru.bars_open.mpv_example.mvp.models.ProductModel;
import ru.bars_open.mpv_example.mvp.views.IProductView;

public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {

    @Inject
    ProductModel mModel;

    private ProductDto mProduct;

    public ProductPresenter(ProductDto product) {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
        mProduct = product;
    }

    @Override
    public void initView() {
        if (getView() == null) return;
        getView().showProductView(mProduct);
    }

    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        mModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 0) {
            mProduct.deleteProduct();
            mModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }
    }

    //region ======================== DI ========================

    private Component createDaggerComponent() {
        return DaggerProductPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductModel provideProductModel() {
            return new ProductModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @ProductScope
    interface Component {
         void inject(ProductPresenter presenter);
    }

    //endregion
}
