package ru.bars_open.mpv_example.mvp.models;


import java.util.List;

import ru.bars_open.mpv_example.data.storage.dto.ProductDto;

public class CatalogModel extends AbstractModel {

    public CatalogModel() {

    }

    public List<ProductDto> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }
}
