package ru.bars_open.mpv_example.mvp.models;


import javax.inject.Inject;

import ru.bars_open.mpv_example.data.managers.DataManager;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.components.DaggerModelComponent;
import ru.bars_open.mpv_example.di.components.ModelComponent;
import ru.bars_open.mpv_example.di.modules.ModelModule;

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
