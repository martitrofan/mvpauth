package ru.bars_open.mpv_example.mvp.presenters;


import java.util.List;

import javax.inject.Inject;

import dagger.Provides;
import ru.bars_open.mpv_example.data.storage.dto.ProductDto;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.scopes.CatalogScope;
import ru.bars_open.mpv_example.mvp.models.CatalogModel;
import ru.bars_open.mpv_example.mvp.views.ICatalogView;
import ru.bars_open.mpv_example.mvp.views.IRootView;
import ru.bars_open.mpv_example.ui.activities.RootActivity;

public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {

    @Inject
    RootPresenter mRootPresenter;

    @Inject
    CatalogModel mModel;

    private List<ProductDto> mProductDtoList;

    public CatalogPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if (mProductDtoList == null) {
            mProductDtoList = mModel.getProductList();
        }
        if (getView() == null) return;
        getView().showCatalogView(mProductDtoList);
    }

    @Override
    public void clickOnBuyButton(int position) {
        if (getView() == null) return;
        if (checkUserAuth()) {
            getRootView().showMessage("Товар " + mProductDtoList.get(position).getProductName() + " успешно добавлен в корзину");
            getView().showCartIndicator();
        } else {
            getView().showAuthScreen();
        }
    }

    private IRootView getRootView() {
        return mRootPresenter.getView();
    }

    @Override
    public boolean checkUserAuth() {
        return mModel.isUserAuth();
    }

    //region ======================== DI ========================

    private Component createDaggerComponent() {
        return DaggerCatalogPresenter_Component.builder()
                .component(DaggerService.getComponent(RootActivity.Component.class))
                .module(new Module())
                .build();
    }

    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogPresenter presenter);
    }

    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }
    }

    //endregion
}
