package ru.bars_open.mpv_example.mvp.views;


import android.support.annotation.Nullable;

import ru.bars_open.mpv_example.ui.custom_views.AuthPanel;

public interface IAuthView extends IRootView {

    void showLoginBtn();
    void hideLoginBtn();

    @Nullable
    AuthPanel getAuthPanel();

    void animateVkBtn();
    void animateFacebookBtn();
    void animateTwitterBtn();

    void showCatalogScreen();
}
