package ru.bars_open.mpv_example.mvp.views;

public interface IRootView extends IView {

    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();
}
