package ru.bars_open.mpv_example.mvp.presenters;

public interface ICatalogPresenter {

    void clickOnBuyButton(int currentItem);
    boolean checkUserAuth();
}
