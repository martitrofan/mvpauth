package ru.bars_open.mpv_example.data.managers;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.bars_open.mpv_example.MvpApplication;
import ru.bars_open.mpv_example.data.network.RestService;
import ru.bars_open.mpv_example.data.storage.dto.ProductDto;
import ru.bars_open.mpv_example.di.DaggerService;
import ru.bars_open.mpv_example.di.components.DaggerDataManagerComponent;
import ru.bars_open.mpv_example.di.components.DataManagerComponent;
import ru.bars_open.mpv_example.di.modules.LocalModule;
import ru.bars_open.mpv_example.di.modules.NetworkModule;


public class DataManager {

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;

    private List<ProductDto> mMockProductList;

    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(MvpApplication.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);

        generateMockData();
    }

    public ProductDto getProductById(int productId) {
        // TODO: 30.10.2016 this temp sample mock data fix me (may be load from db)
        return mMockProductList.get(productId);
    }

    public void updateProduct(ProductDto product) {
        // TODO: 30.10.2016 update product count or status (something in product) save in db
    }

    public List<ProductDto> getProductList() {
        // TODO: 30.10.2016 load product list from anywhere
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "Радио", "https://pp.vk.me/c637326/v637326383/1bd08/HEXVxtttKZg.jpg", "description description description description description description description description description description ", 100, 1));
        mMockProductList.add(new ProductDto(2, "Граммофон", "https://pp.vk.me/c637326/v637326485/190d0/LR8XGV_8EPo.jpg", "description description description description description description description description description description ", 200, 1));
        mMockProductList.add(new ProductDto(3, "Фотоаппарат", "https://pp.vk.me/c637326/v637326593/1d95a/MhDo9nFkLBk.jpg", "description description description description description description description description description description ", 300, 1));
        mMockProductList.add(new ProductDto(4, "Гитара", "https://pp.vk.me/c637326/v637326628/19427/gxLbD5Yv1gY.jpg", "description description description description description description description description description description ", 400, 1));
        mMockProductList.add(new ProductDto(5, "Чемодан", "https://pp.vk.me/c637326/v637326418/1cdcd/MU9BvlWEKkI.jpg", "description description description description description description description description description description ", 500, 1));
        mMockProductList.add(new ProductDto(6, "Микрофон", "https://pp.vk.me/c637326/v637326740/1d00d/X4YKoVmmjsg.jpg", "description description description description description description description description description description ", 600, 1));
        mMockProductList.add(new ProductDto(7, "Телефон", "https://pp.vk.me/c637326/v637326383/1bcf7/fSEi20_lk9A.jpg", "description description description description description description description description description description ", 700, 1));
        mMockProductList.add(new ProductDto(8, "Наручные часы", "https://pp.vk.me/c637326/v637326736/1fa0d/M73dvqEBVzQ.jpg", "description description description description description description description description description description ", 800, 1));
        mMockProductList.add(new ProductDto(9, "Фен", "https://pp.vk.me/c637326/v637326881/21d17/uiDbnllL_cc.jpg", "description description description description description description description description description description ", 900, 1));
    }

    public boolean isAuthUser() {
        // TODO: 10.11.2016 save authToken in preferencesManager
        return false;
        //!mPreferencesManager.getAuthToken().isEmpty();
    }
}
